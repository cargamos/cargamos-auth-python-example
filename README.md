# authentication-example

This project is for demonstrating with an example of getting a token for Cargamos API

## Requeriments
- Python 3.8
- Pip 20

## To request to Cargamos
- Credentials json file
- Target aud id

RUN:
pip install -r requirements.txt