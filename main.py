# %%
import google.auth
from google.auth.transport.requests import Request
import google.oauth2.service_account

IAM_SCOPE = 'https://www.googleapis.com/auth/iam'
OAUTH_TOKEN_URI = 'https://www.googleapis.com/oauth2/v4/token'


def get_google_open_id_connect_token(target_audience, sa_path):
   credentials = google.oauth2.service_account.Credentials.from_service_account_file(sa_path)
   bootstrap_credentials = credentials.with_scopes([IAM_SCOPE])

   bootstrap_credentials.refresh(Request())

   signer_email = bootstrap_credentials.service_account_email
   signer = bootstrap_credentials.signer

   service_account_credentials = google.oauth2.service_account.Credentials(
      signer, signer_email, token_uri=OAUTH_TOKEN_URI, additional_claims={
         'target_audience': target_audience
      })

   service_account_jwt = (service_account_credentials._make_authorization_grant_assertion())
   request = google.auth.transport.requests.Request()
   body = {
      'assertion': service_account_jwt,
      'grant_type': google.oauth2._client._JWT_GRANT_TYPE,
   }
   token_response = google.oauth2._client._token_endpoint_request(request, OAUTH_TOKEN_URI, body)
   return token_response['id_token']


def main():
   jwt = get_google_open_id_connect_token('target_audience', 'path_to/service_account.json')
   print(jwt)


# target_audience and service account, request to Cargamos
main()

if __name__ == '__main__':
    main()
